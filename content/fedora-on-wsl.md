+++
title = "Running Fedora on WSL"
description = "While the basiscs of getting a current Fedora to be used as a WSL(2) distro are already explained in many other sources, I hit issues with locales and man pages. This is a short guide on how I fixed them."
date = 2021-02-06
[taxonomies]
tags = ["linux", "windows", "wsl", "fedora"]
+++

There are already several guides on how to get a minimal Fedora based distro for [WSL](https://docs.microsoft.com/en-us/windows/wsl/) and I recommend following this one, posted on Fedora Magazine, as it is a well written and concise guide to get up and running:

[Using Fedora 33 with Microsoft’s WSL2](https://fedoramagazine.org/wsl-fedora-33/)

With that done, I found a few issues on my instance that origin from using a real minimal rootfs designed to be used in a OCI image.
There where no locales except `C.UTF-8` and thus tools where complaining about missing locales and additionally there where no `man` pages. 

<!-- more -->

## Fixing the locales

To get the locales to work we need two things: configure the correct locales, and install the required files. 
What I did was editing `/etc/locale.conf` to look like this:

```
LANG="en_DK.UTF-8"
LC_ALL="en_DK.UTF-8"
```

> Wondering about the `en_DK` locale? Thats a cheeky hack to get english language, but "central european" date/time/number formats etc. 
> See for example [this unix.stackexchange.com question](https://unix.stackexchange.com/questions/62316/why-is-there-no-euro-english-locale) for why.

Setting `LC_ALL` here is, as far as I know, not optimal, but `/etc/profile.d/lang.sh` is overriding `LANG` with `en_US.UTF-8` for me. Not sure on how to fix this correctly, as `localectl` will not work on systems that are not booted using `systemd`, which WSL instances are not.

This would set the correct locale when opening a shell, but will cause more warnings to appear because our systems currently does not know about english locales. Installing the `en` langpack solves that:

```
$ sudo dnf install -y glibc-langpack-en
```

After that, when opening a new shell, you should have the correct locales set and tools should behave nicely:
```
$ date
2021-02-06T15:19:39 CET
```

## Getting some help: man pages

The problem that took me longer to solve was the lack of man pages. Reinstalling `man-db`, `man-pages` or even everything did not change anything. Calling `mandb` manually procudes nothing, as there where no files in `/usr/share/man`. 

After some complaining and ranting I looked at the GitHub repository for the docker image that provided the rootfs. And ther it way, the correct solution: [fedora-cloud/docker-brew-fedora: no man pages](https://github.com/fedora-cloud/docker-brew-fedora/issues/9). 

For this image, dnf is configured to not install any documentation. Which makes sense for a container image. 
So to fix this, we just need to modify the `/etc/dnf/dnf.conf`: 
```diff
  [main]
  gpgcheck=1
  installonly_limit=3
  clean_requirements_on_remove=True
  best=False
  skip_if_unavailable=True
- tsflags=nodocs
```

After that, reinstall everything for good measure: `$ sudo dnf reinstall -y '*'`

I was required to restart the shell, but after that, it worked! `man man` did indeed display the `man` page for `man`. 
Problems solved. 

## Bonus: backup

Another nice trick I found in [this dev.to post](https://dev.to/bowmanjd/install-fedora-on-windows-subsystem-for-linux-wsl-4b26) was creating a new rootfs archive from the now 'fixed' system.

For this, clean up the image to save disk space in the archive:
``` 
$ sudo dnf clean all
```

Then, in a powershell, create an archive from the distribution: 
```powershell
> wsl --export fedora $HOME\Downloads\fedora-wsl.tar
```

From that archive, one can create a new instance using the `--import` option. 
